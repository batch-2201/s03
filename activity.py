# 1. Create a car dictionary with the following keys: brand, model, year of make, color (any values)

# 2. Print the following statement from the details: "I own a (color) (brand) (model) and it was made in (year_of_make)"

# 3. Create a function that gets the square of a number

# 4. Create a function that takes one of the following languages as its parameter:

# a. French
# b. Spanish
# c. Japanese

# Depending on which language is given, the function prints "Hello World!" in that language. Add a condition that asks the user to input a valid language if the given parameter does not match any of the above.

#solution

#1)

car_dic = { "brand": "Toyota", "model": "Vios", "year_of_make": 2008, "color": "white"}

#2)

print(f"I own a {car_dic['brand']} {car_dic['model']}, and it was made in {car_dic['year_of_make']}")


#3)

#def sqr(num):
 #   return num * num
#print(sqr(11))

def sqrt(num):
	return num * num
print(sqrt(2))

#4 

lang = input("input a language:\n")

def translate_language(convert_language):

    if convert_language == "French":
        print("Comment ça va")
    elif convert_language == "Spanish" :
        print("cómo estás")
    elif convert_language == "Japanese":
        print("Genkidesuka")
    else:
        print("Invalid Converted Language")

translate_language(lang)
